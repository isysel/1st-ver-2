#include "stdafx.h"
#include "figure.h"
#include "ball.h"
#include "paral.h"
#include <fstream>
using namespace std;

void OutBall(ball *a, ofstream &ofst);
void OutParal(paral *a, ofstream &ofst);
ball *InBall(ifstream &ifst);
paral *InParal(ifstream &ifst);

void Out(figure *a,ofstream &ofst)
{
	if (a->key == BALL)
	{
		ofst << "���\n";
		OutBall((ball*)a->element, ofst);
	}
	else if (a->key == PARAL)
	{
		ofst << "��������������: \n";
		OutParal((paral*)a->element, ofst);
	}
}

void In(figure *add, ifstream &ifst)
{
	int key;
	ifst >> key;
	if (key == 1)
	{
		add->key = BALL;
		add->element = (void*)InBall(ifst);
	}
	else if (key == 2)
	{
		add->key = PARAL;
		add->element = (void*)InParal(ifst);
	}
}